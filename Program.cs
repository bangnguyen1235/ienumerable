﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BT_IEnumerable
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Module module1 = new Module() { Name = "Thanh Bang", EmployeeId = 1234, Id = Guid.NewGuid(), Date = DateTime.Now.ToString(), Status = "Active", CompletedDate = DateTime.Now.AddDays(30).ToString() };
            Module module2 = new Module() { Name = "LONG", EmployeeId = 342, Id = Guid.NewGuid(), Date = DateTime.Now.ToString(), Status = "Active", CompletedDate = DateTime.Now.AddDays(30).ToString() };
            Module module3 = new Module() { Name = "lAN", EmployeeId = 5454, Id = Guid.NewGuid(), Date = DateTime.Now.ToString(), Status = "Active", CompletedDate = DateTime.Now.AddDays(30).ToString() };
            Module module4 = new Module() { Name = "hOA", EmployeeId = 342, Id = Guid.NewGuid(), Date = DateTime.Now.ToString(), Status = "Active", CompletedDate = DateTime.Now.AddDays(30).ToString() }; ;

            List<Module> modules = new List<Module>();
            Modules moduleNew = new Modules(modules);
            moduleNew.Add(module1);
            modules.Add(module2);
            modules.Add(module3);
            modules.Add(module4);

            foreach ( Module module in moduleNew) {
                Console.WriteLine(module.Name);
            }


            /// Dictionary
            /// 

            Dictionary<int, Module> di_modules = new Dictionary<int, Module>();
            di_modules.Add(module1.Id.GetHashCode(), module1);
            di_modules.Add(module2.Id.GetHashCode(), module2);
            di_modules.Add(module3.Id.GetHashCode(), module3);
            di_modules.Add(module4.Id.GetHashCode(), module4);



        }
    }
}
