﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BT_IEnumerable
{
    internal class Modules : IEnumerable
    {
        private List<Module> _moduels = null;
        public Modules(List<Module> module) { 
            _moduels = module ;
        }
        public IEnumerator GetEnumerator()
        {
            return _moduels.GetEnumerator();
        }
        public void Add(Module modulue) { 
            _moduels.Add(modulue);
        }
        public void Remove(Module modulue) { 
           _moduels.Remove(modulue);
        }
    }
}
