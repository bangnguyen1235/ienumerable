﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BT_IEnumerable
{
    internal class Module
    {
        
        private Guid _id;

        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private int _employeeId;

        public int EmployeeId
        {
            get { return _employeeId; }
            set { _employeeId = value; }
        }

        private string _date;

        public string Date
        {
            get { return _date; }
            set { _date = value; }
        }
        private string _completedDate;
        public string CompletedDate
        {
            get { return _completedDate; }
            set { _completedDate = value; }
        }

        private string _status;
        public string Status
        {
            get { return _status; }
            set
            {
                if (value == "Active" || value == "Pending" || value == "Inactive") {
                    _status = value;
                }
                else return;
            }
        }

    }
}
